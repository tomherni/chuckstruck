# ChuckStruck

## Prerequisites

Make sure to have the following installed on your development machine:

- [Node.js and NPM](https://nodejs.org/en/download/)
- [MongoDB](https://www.mongodb.com/download-center)

## Installation

Within the root directory, run the command below to download all dependencies:

```
$ npm install
```

Have a local MongoDB instance running on its default port (27017). After installing, it can be started using the command below:

```
$ mongod
```

The server should start on [http://localhost:3000/](http://localhost:3000/).
