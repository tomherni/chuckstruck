'use strict';

process.env.NODE_ENV = 'testing';

/**
 * Module dependencies
 */
const chai = require('chai'),
	should = chai.should(),
	chaiHttp = require('chai-http'),
	mongoose = require('mongoose'),
	server = require('../server'),
	Joke = require('../app/models/joke'),
	jokesData = require('./data/jokes');

chai.use(chaiHttp);

/**
 * Tests for the jokes CRUD methods.
 */
describe('Jokes CRUD Tests:', () => {
	beforeEach(done => {
		Joke.create(jokesData.jokes5, () => {
			done();
		});
	});

	afterEach(done => {
		Joke.collection.drop(() => {
			done();
		});
	});

	it('should be able to get a list of random jokes.', done => {
		chai.request(server)
			.get('/jokes/get-random-jokes')
			.end((err, res) => {
				res.should.have.status(200);
				res.should.be.json;
				res.body.value[0].should.be.a('object');
				res.body.value[0].should.have.property('id');
				res.body.value[0].should.have.property('joke');
				done();
			});
	});

	it('should be able to get a list of favourite jokes.', done => {
		chai.request(server)
			.get('/jokes/get-favourite-jokes')
			.end((err, res) => {
				res.should.have.status(200);
				res.should.be.json;
				res.body[0].should.be.a('object');
				res.body[0].should.have.property('id');
				res.body[0].should.have.property('joke');
				done();
			});
	});

	it('should be able to add a favourite joke.', done => {
		chai.request(server)
			.post('/jokes/add-favourite-joke')
			.send(jokesData.joke1)
			.end((err, res) => {
				res.should.have.status(200);
				res.should.be.json;
				res.body[0].should.be.a('object');
				res.body[0].should.have.property('id');
				res.body[0].should.have.property('joke');
				res.body[0].id.should.equal(jokesData.joke1[0].id);
				res.body[0].joke.should.equal(jokesData.joke1[0].joke);

				chai.request(server)
					.get('/jokes/get-favourite-jokes')
					.end((err, res) => {
						res.should.have.status(200);
						res.should.be.json;
						res.body.should.have.lengthOf(jokesData.jokes5.length + 1);
						res.body[ res.body.length - 1 ].id.should.equal(jokesData.joke1[0].id);
						done();
					});
			});
	});

	it('should be able to delete a favourite joke.', done => {
		let id = jokesData.jokes5[0].id;

		chai.request(server)
			.delete('/jokes/delete-favourite-joke/' + id)
			.end((err, res) => {
				res.should.have.status(200);
				res.should.be.json;
				res.body.n.should.equal(1);

				chai.request(server)
					.get('/jokes/get-favourite-jokes')
					.end((err, res) => {
						res.should.have.status(200);
						res.should.be.json;
						res.body[0].should.not.equal(jokesData.jokes5[0].id);
						res.body.should.have.lengthOf(jokesData.jokes5.length - 1);
						done();
					});
			});
	});
});
