'use strict';

let joke1 = [
	{
		"id": 602,
		"joke": "Chuck Norris doesn't age, because time cannot keep up with him.",
	}
];

let jokes2 = [
	{
		"id": 509,
		"joke": "Chuck Norris's OSI network model has only one layer - Physical."
	},
	{
		"id": 425,
		"joke": "Chuck Norris is the only known mammal in history to have an opposable thumb. On his penis."
	}
];

let jokes3 = [
	{
		"id": 302,
		"joke": "Chuck Norris doesn't go on the internet, he has every internet site stored in his memory. He refreshes webpages by blinking."
	},
	{
		"id": 81,
		"joke": "Chuck Norris invented his own type of karate. It's called Chuck-Will-Kill."
	},
	{
		"id": 378,
		"joke": "There are no such things as tornados. Chuck Norris just hates trailer parks."
	}
];

let jokes4 = [
	{
		"id": 314,
		"joke": "July 4th is Independence day. And the day Chuck Norris was born. Coincidence? I think not."
	},
	{
		"id": 55,
		"joke": "Most people have 23 pairs of chromosomes. Chuck Norris has 72... and they're all poisonous."
	},
	{
		"id": 73,
		"joke": "Chuck Norris doesn't actually write books, the words assemble themselves out of fear."
	},
	{
		"id": 284,
		"joke": "Chuck Norris' first job was as a paperboy. There were no survivors."
	}
];

let jokes5 = [
	{
		"id": 317,
		"joke": "Chuck Norris was once in a knife fight, and the knife lost."
	},
	{
		"id": 93,
		"joke": "When you're Chuck Norris, anything + anything is equal to 1. One roundhouse kick to the face."
	},
	{
		"id": 535,
		"joke": "Chuck Norris uses canvas in IE."
	},
	{
		"id": 501,
		"joke": "Chuck Norris' programs never exit, they terminate."
	},
	{
		"id": 329,
		"joke": "There are only two things that can cut diamonds: other diamonds, and Chuck Norris."
	}
];

let jokes6 = [
	{
		"id": 82,
		"joke": "When an episode of Walker Texas Ranger was aired in France, the French surrendered to Chuck Norris just to be on the safe side."
	},
	{
		"id": 348,
		"joke": "There?s an order to the universe: space, time, Chuck Norris.... Just kidding, Chuck Norris is first."
	},
	{
		"id": 66,
		"joke": "Chuck Norris can't finish a &quot;color by numbers&quot; because his markers are filled with the blood of his victims. Unfortunately, all blood is dark red."
	},
	{
		"id": 539,
		"joke": "Chuck Norris's database has only one table, 'Kick', which he DROPs frequently."
	},
	{
		"id": 340,
		"joke": "A man once claimed Chuck Norris kicked his ass twice, but it was promptly dismissed as false - no one could survive it the first time."
	},
	{
		"id": 89,
		"joke": "For some, the left testicle is larger than the right one. For Chuck Norris, each testicle is larger than the other one."
	}
];

let jokes10 = [
	{
		"id": 375,
		"joke": "After taking a steroids test doctors informed Chuck Norris that he had tested positive. He laughed upon receiving this information, and said &quot;of course my urine tested positive, what do you think they make steroids from?&quot;"
	},
	{
		"id": 103,
		"joke": "Chuck Norris and Mr. T walked into a bar. The bar was instantly destroyed, as that level of awesome cannot be contained in one building."
	},
	{
		"id": 476,
		"joke": "Chuck Norris doesn't need a debugger, he just stares down the bug until the code confesses."
	},
	{
		"id": 110,
		"joke": "You know how they say if you die in your dream then you will die in real life? In actuality, if you dream of death then Chuck Norris will find you and kill you."
	},
	{
		"id": 290,
		"joke": "There are two types of people in the world... people that suck, and Chuck Norris."
	},
	{
		"id": 316,
		"joke": "In the medical community, death is referred to as &quot;Chuck Norris Disease&quot;"
	},
	{
		"id": 195,
		"joke": "Chuck Norris wears a live rattlesnake as a condom."
	},
	{
		"id": 310,
		"joke": "182,000 Americans die from Chuck Norris-related accidents every year."
	},
	{
		"id": 546,
		"joke": "Chuck Norris does infinit loops in 4 seconds."
	},
	{
		"id": 76,
		"joke": "If tapped, a Chuck Norris roundhouse kick could power the country of Australia for 44 minutes."
	}
];

module.exports = { joke1, jokes2, jokes3, jokes4, jokes5, jokes6, jokes10 };
