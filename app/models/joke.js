'use strict';

/**
 * Module dependencies.
 */
const mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Joke schema.
 */
let jokeSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	id: {
		type: Number,
		unique: true,
		trim: true,
		required: 'Joke ID cannot be empty.'
	},
	joke: {
		type: String,
		trim: true,
		required: 'Joke cannot be empty.'
	}
});

module.exports = mongoose.model('Jokes', jokeSchema);
