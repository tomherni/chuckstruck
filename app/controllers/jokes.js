'use strict';

/**
 * Module dependencies.
 */
const request = require('request'),
	Joke = require('../models/joke');

/**
 * Get 10 random jokes.
 */
exports.getRandomJokes = (req, res) => {
	let amount = Number(req.params.amount) || 10;

	request('http://api.icndb.com/jokes/random/' + amount, (err, result) => {
		if (err) {
			return res.status(500).send(err);
		} else {
			res.status(200).json( JSON.parse(result.body) );
		}
	});
};

/**
 * Get all favourite jokes.
 */
exports.getFavouriteJokes = (req, res) => {
	Joke.find().sort('created').exec((err, result) => {
		if (err) {
			return res.status(500).send(err);
		} else {
			res.status(200).json(result);
		}
	});
};

/**
 * Add a new favourite joke.
 */
exports.addFavouriteJoke = (req, res) => {
	Joke.count({}, (err, count) => {
		if (count < 10) {
			Joke.create(req.body, (err, result) => {
				if (err) {
					return res.status(500).send(err);
				} else {
					res.status(200).json(result);
				}
			});
		} else {
			res.status(429).send('Maximum amount of favourite jokes reached.');
		}
	});
};

/**
 * Delete a favourite joke.
 */
exports.deleteFavouriteJoke = (req, res) => {
	let id = Number(req.params.id) || 0;

	if (id) {
		Joke.find({ id: id }).remove((err, result) => {
			if (err) {
				return res.status(500).send(err);
			} else {
				res.status(200).json(result);
			}
		});
	}
};

/**
 * Delete all favourite jokes.
 */
exports.deleteAllFavouriteJokes = (req, res) => {
	Joke.collection.drop((err) => {
		if (err) {
			return (err.codeName === 'NamespaceNotFound')
				? res.status(200).send(true)
				: res.status(500).send(err);
		} else {
			res.status(200).send(true);
		}
	});
};
