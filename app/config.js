'use strict';

let config = {
	mongoURI: {
		development: 'mongodb://localhost:27017/chuckstruck-development',
		testing: 'mongodb://localhost:27017/chuckstruck-testing'
	}
};

module.exports = config;
