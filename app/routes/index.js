'use strict';

/**
 * Module dependencies.
 */
const express = require('express'),
	jokesController = require('../controllers/jokes');

/**
 * Routes.
 */
module.exports = express.Router()
	.get('/', (req, res) => {
		res.render('index');
	})
	.get('/jokes/get-random-jokes/:amount?', jokesController.getRandomJokes)
	.get('/jokes/get-favourite-jokes', jokesController.getFavouriteJokes)
	.post('/jokes/add-favourite-joke', jokesController.addFavouriteJoke)
	.delete('/jokes/delete-favourite-joke/:id', jokesController.deleteFavouriteJoke)
	.delete('/jokes/delete-all-favourite-jokes', jokesController.deleteAllFavouriteJokes);
