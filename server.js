'use strict';

/**
 * Module dependencies.
 */
const path = require('path'),
	config = require('./app/config'),
	express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	sassMiddleware = require('node-sass-middleware'),
	routes = require('./app/routes/index'),
	http = require('http'),
	mongoose = require('mongoose');

/**
 * Express setup.
 */
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(sassMiddleware({
	src: path.join(__dirname, 'public'),
	dest: path.join(__dirname, 'public'),
	outputStyle: 'compressed',
	indentedSyntax : false,
	sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', routes);

/**
 * Error handling.
 */
app.use((req, res, next) => {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});
app.use((err, req, res, next) => {
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	res.status(err.status || 500);
	res.render('index');
});

/**
 * Server and database configuration.
 */
let port = process.env.PORT || '3000';
app.set('port', port);

mongoose.connect(config.mongoURI[ app.settings.env ], { useMongoClient: true }, (err, res) => {
	if (err) {
		console.log(err);
	} else {
		console.log('Connected to Database: ' + config.mongoURI[ app.settings.env ]);
	}
});

http.createServer(app).listen(port, () => {
	console.log('Server running on port: ' + port);
});

module.exports = app;
