'use strict';

Vue.component('joke-item', {
	props: ['joke'],
	template: `<li class="jokes__joke">
		<span v-html="joke.joke"></span>
		<img
			:src="'images/heart' + ($parent.ids.indexOf(joke.id) >= 0 ? '-active' : '') + '.svg'"
			@click="$parent.toggleFavourite(joke)"
			@keyup.13="$parent.toggleFavourite(joke)"
			class="jokes__favourite"
			alt="Toggle favourite"
			title="Toggle favourite"
			tabindex="0">
	</li>`
});

new Vue({
	el: '.container',
	data: {
		randomJokes: [],
		ids: [],
		favouriteJokes: [],
		autoAdd: false
	},
	created() {
		// Initialize by retrieving the random jokes and favourite jokes.
		this.getRandomJokes();
		this.getFavouriteJokes();
	},
	methods: {
		/**
		 * Get 10 random jokes.
		 */
		getRandomJokes() {
			axios.get('/jokes/get-random-jokes/')
				.then(res => {
					if (typeof res !== 'undefined' && res.status === 200) {
						this.randomJokes = res.data.value;
					}
				})
				.catch(e => {
					console.error(e.message);
				});
		},

		/**
		 * Get all favourite jokes.
		 */
		getFavouriteJokes() {
			axios.get('/jokes/get-favourite-jokes')
				.then(res => {
					if (typeof res !== 'undefined' && res.status === 200) {
						this.favouriteJokes = res.data;

						this.favouriteJokes.forEach(value => {
							this.ids.push(value.id);
						});
					}
				})
				.catch(e => {
					console.error(e.message);
				});
		},

		/**
		 * Add a new favourite joke.
		 *
		 * @param {object} joke
		 */
		addFavouriteJoke(joke) {
			let data = {
				id: joke.id,
				joke: joke.joke
			};

			axios.post('/jokes/add-favourite-joke', data)
				.then(res => {
					if (typeof res !== 'undefined' && res.status === 200) {
						this.favouriteJokes.push(res.data);
						this.ids.push(res.data.id);
					}
				})
				.catch(e => {
					console.error(e.message);

					// Stop auto-adding favourite jokes if it is active.
					if (this.autoAdd) {
						this.toggleAutoAdd();
					}
				});
		},

		/**
		 * Delete a favourite joke.
		 *
		 * @param {object} joke
		 * @param {number} index
		 */
		deleteFavouriteJoke(joke, index) {
			axios.delete('/jokes/delete-favourite-joke/' + joke.id)
				.then(res => {
					if (typeof res !== 'undefined' && res.status === 200) {
						this.favouriteJokes.splice(index, 1);
						this.ids.splice(index, 1);
					}
				})
				.catch(e => {
					console.error(e.message);
				});
		},

		/**
		 * Add a joke as a favourite, or delete it if it is already added as a favourite.
		 *
		 * @param {object} joke
		 */
		toggleFavourite(joke) {
			let index = this.ids.indexOf(joke.id);

			if (index >= 0)
				this.deleteFavouriteJoke(joke, index);
			else
				this.addFavouriteJoke(joke);
		},

		/**
		 * Set an interval to automatically add a random joke to the favourites.
		 */
		toggleAutoAdd() {
			if (this.autoAdd) {
				clearInterval(this.autoAdd);
				this.autoAdd = false;
			} else {
				this.autoAdd = setInterval(() => {
					this.autoAddRandomJoke();
				}, 5000);
			}
		},

		/**
		 * Automatically add a random joke to the favourites.
		 */
		autoAddRandomJoke() {
			axios.get('/jokes/get-random-jokes/1')
				.then(res => {
					if (typeof res !== 'undefined' && res.status === 200) {
						this.addFavouriteJoke(res.data.value[0]);
					}
				})
				.catch(e => {
					console.error(e.message);
				});
		}
	}
});
